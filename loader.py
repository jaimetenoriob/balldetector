import cv2
import csv
import torch
import numpy as np

def image_to_tensor(path):
    im = cv2.imread(path, 0)
    im = im.reshape(1, 1, 100, 100)
    tensor = torch.tensor(im).float()/255
    return tensor

def load_dataset(data_path="train"):
    images = []
    labels = []
    with open(f"data/{data_path}/labels.csv", "r") as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=["filename", "present", "left", "top", "right", "bottom"])

        for idx, row in enumerate(reader):
            if not idx == 0:
                path = row["filename"]
                image = image_to_tensor(path)
                images.append(image)

                label = torch.tensor([[
                    float(row["present"]),
                    float(row["left"]),
                    float(row["top"]),
                    float(row["right"]),
                    float(row["bottom"])
                ]], dtype=torch.float)
                labels.append(label)

    return images, labels
