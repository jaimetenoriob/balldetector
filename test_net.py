import numpy as np
import cv2
import torch

from train_net import ConvNet
from generator import generate_circles


def image_to_tensor(im):
    im = im.reshape(1, 1, 100, 100)
    tensor = torch.tensor(im).float()
    return tensor


def main():
    model = ConvNet()
    model.load_state_dict(torch.load("model/ball.pth"))
    model.eval()

    circles, labels = generate_circles(50)

    for circle, (p, l, t, r, b) in zip(circles, labels):
        im = image_to_tensor(circle)
        out_tensor = model.forward(im)
        pp = out_tensor[0][0]
        out = out_tensor*100

        vis = cv2.cvtColor(np.array(circle*255, dtype=np.uint8), cv2.COLOR_GRAY2RGB)
        lp, tp, rp, bp = out[0][1], out[0][2], out[0][3], out[0][4]

        if pp > 0.5:
            cv2.rectangle(vis, (lp, tp), (lp+rp, tp+bp), (0, 255, 0), 2)

        print("P/T")
        print(out_tensor)
        print(p, l, t, r, b)

        cv2.imshow("Prediction", vis)
        cv2.waitKey(0)


if __name__ == '__main__':
    main()
