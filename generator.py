import os
import cv2
import csv
import sys
import glob
import random
import numpy as np

from random import choice

present = lambda : choice([True, True, False])
rand = lambda a, b: np.random.randint(a, b)


def generate_video(length, dim=100):
    white = np.ones((dim, dim))

    frames = []
    ra = rand(3, int(dim/2))
    cx = rand(ra, dim-1-ra)
    cy = rand(ra, dim-1-ra)

    for _ in range(length):
        frame = cv2.circle(white.copy(), (cx, cy), ra, 0, -1)
        frames.append(frame)

        inc = random.randint(-3, 3)
        ra = ra + inc
        ra = np.clip(ra, 3, int(dim/2))

        inc = random.randint(-3, 3)
        cx = cx + inc
        cx = np.clip(cx, ra, dim-1-ra)

        inc = random.randint(-3, 3)
        cy = cy + inc
        cy = np.clip(cy, ra, dim-1-ra)


    fourcc = cv2.VideoWriter_fourcc(*'mp4v') # Be sure to use lower case
    out = cv2.VideoWriter("output.mp4", fourcc, 20.0, (dim, dim))

    for frame in frames:
        out_frame = cv2.cvtColor(np.array(frame*255, dtype=np.uint8), cv2.COLOR_GRAY2RGB)
        out.write(out_frame)

    out.release()


def generate_circles(num=100, dim=100):
    white = np.ones((dim, dim))

    circles = []
    coords = []
    for _ in range(num):
        if present():
            ra = rand(3, int(dim/2))
            cx = rand(0, dim-1)
            cy = rand(0, dim-1)
            # cx = rand(ra, dim-1-ra)
            # cy = rand(ra, dim-1-ra)

            circle = cv2.circle(white.copy(), (cx, cy), ra, 0, -1)
            circles.append(circle)

            l = (cx - ra)/dim
            t = (cy - ra)/dim
            r = (cx + ra)/dim
            b = (cy + ra)/dim

            coords.append((1.0, l, t, r, b))

        else:
            circles.append(white.copy())
            coords.append((0.0, 0.0, 0.0, 0.0, 0.0))



    return circles, coords


def main(num=100, dim=100, path="train"):
    fieldnames = ["filename", "present", "left", "top", "right", "bottom"]
    with open(f"data/{path}/labels.csv", "w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

    [os.remove(file) for file in glob.glob(f"data/{path}/images/*")]

    circles, coords = generate_circles(num, dim)
    for idx, (circle, (p, l, t, r, b)) in enumerate(zip(circles, coords)):
        filename = f"data/{path}/images/{idx:04}.jpg"
        cv2.imwrite(filename, circle*255)

        with open(f"data/{path}/labels.csv", "a", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow({
                "filename": filename,
                "present": p,
                "left": l,
                "top": t,
                "right": r,
                "bottom": b
            })

        print(f"{idx:04}/{num:04}", end="\r")

    print(f"\nGENERATED {num} IMAGES for {path}")


if __name__ == "__main__":
    main()
