from torch import nn, cat
from torch.nn import functional as F

class ConvNet(nn.Module):
    def __init__(self):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 8, kernel_size=3, stride=1, padding=0),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        self.layer2 = nn.Sequential(
            nn.Conv2d(8, 16, kernel_size=3, stride=1, padding=0),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=1))

        self.layer3 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=0),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))

        # self.layer4 = nn.Sequential(
        #     nn.Conv2d(32, 64, kernel_size=2, stride=2, padding=0),
        #     nn.ReLU(),
        #     nn.MaxPool2d(kernel_size=3, stride=2))

        self.flatten = nn.Flatten()
        self.drop_out = nn.Dropout()

        self.fc1 = nn.Linear(32 * 22 * 22, 100)
        self.fc2 = nn.Linear(100, 5)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = self.layer3(out)
        # out = self.layer4(out)
        # out = out.reshape(out.size(0), -1)
        out = self.flatten(out)
        out = self.drop_out(out)
        out = self.fc1(out)

        out = F.relu(out)

        out = self.fc2(out)
        # return out

        out_a = out[:, 0:1]
        out_b = out[:, 1:]
        out_a = F.sigmoid(out_a)

        out = cat((out_a, out_b), -1)
        return out
