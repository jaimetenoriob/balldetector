import torch
import random

from torch import nn
from model import ConvNet
from loader import load_dataset
from torch.nn import functional as F

def main(num_epochs = 5, learning_rate = 0.001):
    model = ConvNet()

    MSE = nn.MSELoss()
    BCE = nn.BCELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    train_images, train_labels = load_dataset()
    total_step = len(train_labels)

    for epoch in range(num_epochs):
        train_batch = list(zip(train_images.copy(), train_labels.copy()))
        random.shuffle(train_batch)

        for i, (images, labels) in enumerate(train_batch):

            outputs = model(images)

            pc_pred = outputs[0][0]
            pc_true = labels[0][0]

            pos_pred = outputs[0][1:]
            pos_true = labels[0][1:]

            loss = BCE(pc_pred, pc_true) + pc_true*MSE(pos_pred, pos_true)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if (i + 1) % 20 == 0:
                print("\n" + "-"*50)
                print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(epoch + 1, num_epochs, i + 1, total_step, loss.item()))
                print("Present:")
                print("PRED: {}\nTRUE: {}".format(pc_pred.detach().numpy(), pc_true.numpy()))
                print("Position:")
                print("PRED: {}\nTRUE: {}".format(pos_pred.detach().numpy(), pos_true.numpy()))


    torch.save(model.state_dict(), "model/ball.pth")
    print("SAVED MODEL")


if __name__ == '__main__':
    main()
