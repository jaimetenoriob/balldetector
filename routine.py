import argparse
import generator
import train_net
import test_net

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--generator", "-gen", help="Generate dataset", action="store_true")
    parser.add_argument("--dimension", "-dim", help="Image dimension", type=int, default=100)
    parser.add_argument("--training", "-tr", help="Training samples", type=int, default=500)
    parser.add_argument("--testing", "-te", help="Testing samples", type=int, default=100)
    parser.add_argument("--train", "-Tr", help="Train network", action="store_true")
    parser.add_argument("--epochs", "-ep", help="Training epochs", type=int, default=5)
    parser.add_argument("--learning_rate", "-lr", help="Learning rate", type=float, default=0.001)
    parser.add_argument("--test", "-Te", help="Test network", action="store_true")
    args = parser.parse_args()

    if args.generator:
        generator.main(args.training, args.dimension, "train")
        generator.main(args.testing, args.dimension, "test")

    if args.train:
        train_net.main(args.epochs, args.learning_rate)

    if args.test:
        test_net.main()
